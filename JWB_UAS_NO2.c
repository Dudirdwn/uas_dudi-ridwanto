#include <stdio.h>

// Deklarasi fungsi
double hitungNilai(double quiz, double absen, double uts, double uas, double tugas, 
double p_quiz, double p_absen, double p_uts, double p_uas, double p_tugas);

void cetakMutu(double nilai);

int main() {

  double quiz, absen, uts, uas, tugas;
  double p_quiz, p_absen, p_uts, p_uas, p_tugas;
  
  double nilai;
  char hurufMutu;

  // Input nilai 
  printf("Masukkan nilai quiz: ");
  scanf("%lf", &quiz);

  printf("Masukkan nilai absen: ");
  scanf("%lf", &absen);

  printf("Masukkan nilai UTS: ");
  scanf("%lf", &uts);

  printf("Masukkan nilai UAS: ");
  scanf("%lf", &uas);

  printf("Masukkan nilai tugas: ");
  scanf("%lf", &tugas);

  // Input prosentase 
  printf("Masukkan prosentase quiz: ");
  scanf("%lf", &p_quiz);

  printf("Masukkan prosentase absen: ");
  scanf("%lf", &p_absen);

  printf("Masukkan prosentase UTS: ");
  scanf("%lf", &p_uts);

  printf("Masukkan prosentase UAS: ");
  scanf("%lf", &p_uas);

  printf("Masukkan prosentase tugas: ");
  scanf("%lf", &p_tugas);

  // Hitung nilai akhir
  nilai = hitungNilai(quiz, absen, uts, uas, tugas, 
  p_quiz, p_absen, p_uts, p_uas, p_tugas);

  // Cetak mutu
  cetakMutu(nilai);

  return 0;
}

// Fungsi hitung nilai
double hitungNilai(double quiz, double absen, double uts, double uas, double tugas,
double p_quiz, double p_absen, double p_uts, double p_uas, double p_tugas) {
  
  double nilai;

  nilai = (p_quiz * quiz) + (p_absen * absen) + (p_tugas * tugas) + 
  (p_uts * uts) + (p_uas * uas);

  return nilai;

}

// Cetak mutu
void cetakMutu(double nilai) {

  char hurufMutu;

  if (nilai > 85)
    hurufMutu = 'A';
  else if (nilai > 70)
    hurufMutu = 'B';
  else if (nilai > 55)
    hurufMutu = 'C';
  else if (nilai > 40)
    hurufMutu = 'D';
  else
    hurufMutu = 'E';

  printf("Huruf Mutu: %c\n", hurufMutu);

}
